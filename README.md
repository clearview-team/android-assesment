# Android assessment task for Clearview

Create a mobile application for Android which will fetch the following data from this endpoint:

    https://jsonplaceholder.typicode.com/todos

List all todo items from that list in a `ListView` component. All items which are completed dim in a light-green color, whilst uncompleted ones should be left as they are by default.

Create a seamles user experience to grab new todo’s, and request for new ones on-demand.

Enable use of application and listing todo’s in offline.

When a user taps on a todo item, present a custom modal with information: `id` and `userId`.

Introduce code structure and reasonable design patterns. Apply SOLID principles as much as possible.

Preferably, use Java, but Kotlin can go as well. Do not use a `WebView`, PhoneGap/Ionic, or a similar web-rendering approach, since this is not we’re asking from you in this task.

Use native Android/Java apporach with default Android components and theming, without any special custimizations.

Provide this solution in a GitHub repository, either public of private, and invite Clearview for access so we can review it.

Create a distributable APK with both development and release builds, along with proper signing keys for keystore.

Bonuses:

 - Use enable toggling between dark and light mode
 - Use system-wide settings for dark/light mode
